#include <iostream>
#include <tuple>

template<class _function> class constexpr_lambda_functor {
private:
	const _function f;
public:
	constexpr constexpr_lambda_functor(const _function & _f) : f(_f) {};
	constexpr constexpr_lambda_functor() = delete;
	~constexpr_lambda_functor() = default;

	template<typename ...Args> constexpr auto operator () (const Args && ..._args) const {
		return f(std::forward<std::tuple<Args...>>(std::make_tuple(_args...)));
	}
};

template<class _tuple_args, class _function> constexpr auto make_lambda(const _function & _f ) {
	return constexpr_lambda_functor<_function>(_f);
}

#define LAMBDA(NAME) constexpr auto tuple_catch_##NAME = 
#define ARGS_CATCH_BEGIN std::make_tuple(
#define ARGS_CATCH_END );
#define WITH_ARGS(NAME) using tuple_arg_##NAME = 
#define ARGS_FUNC_BEGIN std::tuple<
#define ARGS_FUNC_END >;
#define BODY(NAME, body_code) struct functor_type_##NAME { \
	private: \
	const decltype(tuple_catch_##NAME) & catch_buf; \
	public: \
	constexpr functor_type_##NAME(decltype(tuple_catch_##NAME) & _catch_buf) : catch_buf(_catch_buf) {}; \
	constexpr auto operator () (const tuple_arg_##NAME && _args) const \
		body_code \
}; \
constexpr functor_type_##NAME functor_##NAME(tuple_catch_##NAME); \
auto lambda_##NAME = make_lambda<tuple_arg_##NAME>(functor_##NAME);

template<int i> struct TEMPLATE {
	static void perform (void) {
		std::cout << i << "\n";
	}
};

int main()
{

	constexpr int N = 10;
	constexpr double A = 0.25;
	constexpr char S[] = "Char String\n";

	LAMBDA(TEST) //TEST is name, name of lambda will be lambda_TEST
		ARGS_CATCH_BEGIN N, A, S ARGS_CATCH_END //Arguments to catch, numeration from 0, access std::get< index >(catch_buf)
	WITH_ARGS(TEST)
		ARGS_FUNC_BEGIN int, double ARGS_FUNC_END //Arguments of functor, numeration from 0, access std::get< index >(_args)
		BODY(TEST, 
			{ 
				return std::get<0>(_args); 
			})

	TEMPLATE<lambda_TEST(97, 3.)>::perform(); //result of lambda_TEST(97, 3.) is constexpr value 97
}